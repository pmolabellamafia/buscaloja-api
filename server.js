require("dotenv").config();
const express = require("express");
const app = express();
const cors = require("cors");
const serverless = require('serverless-http');
const request = require("request-promise");
const PORT = process.env.PORT || 3001;
const gs = require("google-spreadsheet");

const id = "1Sj9Jis6Gq07bCWHd3wC_rN1FuDHrih65FySG7IpNvxQ";

app.use(cors());
app.listen(PORT, () => console.log(`Listening on port: ${PORT}`));

const getDoc = async () => {
  try{
    const doc = new gs.GoogleSpreadsheet(id);
  
    await doc.useServiceAccountAuth({
      client_email: process.env.client_email,
      private_key: process.env.private_key.replace(/\\n/g, "\n"),
    });
    await doc.loadInfo();
    return doc;
  } catch(error) {
    res.status(statusCode >= 100 && statusCode < 600 ? err.code : 500)
    throw new Error(err);
  }
};

async function pegarDadosTabela() {
  try{
  const dados = getDoc().then(async (doc) => {
      sheet = doc.sheetsByIndex[0];
      const rows = await sheet.getRows();

      const dados = rows.map((dados) => {
        return dados;
      });
      return dados;
    });
    return dados;
  } catch(error) {
    res.status(statusCode >= 100 && statusCode < 600 ? err.code : 500)
    throw new Error(err);
  }
}

app.get("/", (req, res) => {
  res.send('funfa')
});

app.get("/api/atualiza", async (req, res) => {
  try{
    let sheet;

    list = [];

    async function pegarDadosAPI(cep) {
      try {
        let options1 = {
          uri: `https://viacep.com.br/ws/${cep}/json/`,
          json: true,
        };
        const codCidade = await request(options1)
          .then(function (response) {
            return response.ibge;
          })
          .catch(function (err) {
            console.log(err);
          });

        let options2 = {
          uri: `https://servicodados.ibge.gov.br/api/v1/localidades/municipios/${codCidade}`,
          json: true,
        };
        const resp = await request(options2)
          .then(function (response) {
            return response;
          })
          .catch(function (err) {
            console.log(err);
          });

        return resp;
      } catch(error) {
        res.status(statusCode >= 100 && statusCode < 600 ? err.code : 500)
        throw new Error(err);
      }
    }

    async function atualizaDados(res) {
      try{
        pegarDadosAPI(res.cep).then((d) => {
          res.cidade = d.nome;
          res.microrregiao = d.microrregiao.nome;
          res.mesorregiao = d.microrregiao.mesorregiao.nome;
          res.uf = d.microrregiao.mesorregiao.UF.nome;
        });

        getDoc().then((doc) => {
          sheet = doc.sheetsByIndex[2];
          sheet.getRows().then((rows) => {
            rows.map((row) => {
              if (row.cd_cliente === res.cd_cliente) {
                row.cidade = res.cidade;
                row.microrregiao = res.microrregiao;
                row.mesorregiao = res.mesorregiao;
                row.uf = res.uf;

                row.save().then(() => {
                  console.log("Dado atualizado!");
                });
              }
            });
          });
        });
      } catch(error) {
        res.status(statusCode >= 100 && statusCode < 600 ? err.code : 500)
        throw new Error(err);
      }
    }

    pegarDadosTabela().then((r) => {
      for (i = 0; i < r.length; i++) {
        atualizaDados(r[i]);
      }
    });
    res.status(200).send("ok");
  } catch(error) {
    res.status(statusCode >= 100 && statusCode < 600 ? err.code : 500)
    throw new Error(err);
  }
});

// --------------------------------------------------------------------------------- GET
app.get("/api/", async (req, res) => {
  try {
    let { tracking } = req.query;
    if(tracking.length != 8){
      res.status(400).send('NOK');
      console.log('nok')
      return;
    }

    const doc = new gs.GoogleSpreadsheet(id);

    await doc.useServiceAccountAuth({
      client_email: process.env.client_email,
      private_key: process.env.private_key.replace(/\\n/g, "\n"),
    });

    await doc.loadInfo();

    let options1 = {
      uri: `https://viacep.com.br/ws/${tracking}/json/`,
      json: true,
    };
    const codCidade = await request(options1)
      .then(function (response) {
        return response.ibge;
      })
      .catch(function (err) {
        console.log(err);
        throw new Error(err);
      });

    let options2 = {
      uri: `https://servicodados.ibge.gov.br/api/v1/localidades/municipios/${codCidade}`,
      json: true,
    };
    const result = await request(options2)
      .then(function (response) {
        return response;
      })
      .catch(function (err) {
        console.log(err);
        throw new Error(err);
      });

    let listaLojas = [];

    // pegar todos os dados da tabela
    const rst = await pegarDadosTabela().then((r) => {
      for (i = 0; i < r.length; i++) {
        listaLojas.push({
          cd_cliente: Object.values(r[i])[2][0],
          cliente: Object.values(r[i])[2][1],
          endereco: Object.values(r[i])[2][4],
          cep: Object.values(r[i])[2][5],
          cidade: Object.values(r[i])[2][6], // 4
          microrregiao: Object.values(r[i])[2][7], // 5
          mesorregiao: Object.values(r[i])[2][8], // 6
          uf: Object.values(r[i])[2][9], // 7
        });
      }
    });

    
    // cidade: data.nome,
    // microrregiao: data.microrregiao.nome,
    // mesorregiao: data.microrregiao.mesorregiao.nome,
    // uf: data.microrregiao.mesorregiao.UF.nome,

    let lojas = listaLojas.filter(x => x['cidade'] === result.nome);

    if(lojas.length === 0) {
        lojas = listaLojas.filter(x => x['microrregiao'] === result.microrregiao.nome);
        if(lojas.length === 0) {
            lojas = listaLojas.filter(x => x['mesorregiao'] === result.microrregiao.mesorregiao.nome);
            if(lojas.length === 0) {
                lojas = listaLojas.filter(x => x['uf'] === result.microrregiao.mesorregiao.UF.nome);
            }
        }            
    }
    console.log('ok')
    res.send(lojas);
  } catch(error) {
    res.status(statusCode >= 100 && statusCode < 600 ? err.code : 500)
    throw new Error(err);
  }
});


module.exports = app;
module.exports.handler = serverless(app);